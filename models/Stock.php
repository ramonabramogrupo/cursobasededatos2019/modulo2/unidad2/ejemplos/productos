<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property int $producto
 * @property int $tienda
 * @property int $stock
 * @property double $precioVenta
 *
 * @property Tienda $tienda0
 * @property Producto $producto0
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'tienda', 'stock'], 'integer'],
            [['precioVenta'], 'number'],
            [['producto', 'tienda'], 'unique', 'targetAttribute' => ['producto', 'tienda']],
            [['tienda'], 'exist', 'skipOnError' => true, 'targetClass' => Tienda::className(), 'targetAttribute' => ['tienda' => 'idTienda']],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto' => 'idproducto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producto' => 'Producto',
            'tienda' => 'Tienda',
            'stock' => 'Stock',
            'precioVenta' => 'Precio Venta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTienda0()
    {
        return $this->hasOne(Tienda::className(), ['idTienda' => 'tienda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Producto::className(), ['idproducto' => 'producto']);
    }
}
