<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $idproducto
 * @property int $nombre
 * @property string $foto
 * @property string $descripcion
 * @property double $precio
 * @property string $fechaAlta
 *
 * @property Pedidos[] $pedidos
 * @property Stock[] $stocks
 * @property Tienda[] $tiendas
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'integer'],
            [['descripcion'], 'string'],
            [['precio'], 'number'],
            [['fechaAlta'], 'safe'],
            [['foto'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idproducto' => 'Idproducto',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'fechaAlta' => 'Fecha Alta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['producto' => 'idproducto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['producto' => 'idproducto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiendas()
    {
        return $this->hasMany(Tienda::className(), ['idTienda' => 'tienda'])->viaTable('stock', ['producto' => 'idproducto']);
    }
}
