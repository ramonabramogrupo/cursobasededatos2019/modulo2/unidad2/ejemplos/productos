﻿DROP DATABASE IF EXISTS productos;
CREATE DATABASE productos;
USE productos;
CREATE OR REPLACE TABLE producto(
  idproducto int AUTO_INCREMENT,
  nombre int,
  foto varchar(150),
  descripcion text,
  precio float,
  fechaAlta date,
  PRIMARY KEY (idproducto)
  );

CREATE OR REPLACE TABLE tienda(
  idTienda int AUTO_INCREMENT,
  direccion varchar(100),
  poblacion varchar(50),
  telefono varchar(20),
  email varchar(50),
  PRIMARY KEY(idTienda)
 );

CREATE OR REPLACE TABLE stock(
  id int AUTO_INCREMENT,
  producto int,
  tienda int,
  stock int,
  precioVenta float,
  PRIMARY KEY(id),
  UNIQUE KEY(producto,tienda),
  CONSTRAINT FKstockproducto FOREIGN KEY (producto) REFERENCES producto(idproducto) ON DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT FKstockTienda FOREIGN KEY (tienda) REFERENCES tienda(idTienda) on DELETE CASCADE on UPDATE CASCADE
  );

CREATE OR REPLACE TABLE pedidos(
  idPedido int AUTO_INCREMENT,
  producto int,
  tienda int,
  precioCompra float,
  cantidad int,
  fechaPedido date,
  horaPedido time,
  UNIQUE KEY(producto,tienda,fechaPedido,horaPedido),
  PRIMARY KEY(idPedido),
  CONSTRAINT FKpedidosproducto FOREIGN KEY (producto) REFERENCES producto(idproducto) ON DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT FKpedidosTienda FOREIGN KEY (tienda) REFERENCES tienda(idTienda) on DELETE CASCADE on UPDATE CASCADE
  );


