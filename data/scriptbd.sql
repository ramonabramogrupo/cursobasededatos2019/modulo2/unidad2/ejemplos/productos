﻿
-- crear base de datos
DROP DATABASE IF EXISTS productos;

CREATE DATABASE productos
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

USE productos;

-- crear tablas

CREATE TABLE producto (
  idproducto int AUTO_INCREMENT PRIMARY KEY,
  nombre int,
  foto varchar(150),
  descripcion text,
  precio float(4),
  fechalta date);

CREATE TABLE tienda (
  idTienda int AUTO_INCREMENT PRIMARY KEY,
  direccion varchar(100),
  poblacion varchar(50),
  telefono varchar(20),
  email varchar(50));

CREATE TABLE stock (
  id int AUTO_INCREMENT PRIMARY KEY,
  producto int,
  tienda int,
  stock int);

CREATE TABLE pedidos(
  idPedido int AUTO_INCREMENT PRIMARY KEY,
  producto int,
  tienda int,
  precioCompra float(4),
  cantidad int,
  fechaPedido date,
  horaPedido time);

ALTER TABLE pedidos 
  ADD CONSTRAINT FKpedidosTienda FOREIGN KEY (tienda) REFERENCES tienda(idTienda),
  ADD CONSTRAINT FKpedidosproductos FOREIGN KEY (producto) REFERENCES producto(idproducto),
  ADD CONSTRAINT producto UNIQUE (producto, tienda, fechaPedido, horaPedido);

ALTER TABLE stock
  ADD CONSTRAINT FKstockproducto FOREIGN KEY (tienda) REFERENCES tienda(idTienda),
  ADD CONSTRAINT FKstockTienda FOREIGN KEY (producto) REFERENCES producto(idproducto),
  ADD CONSTRAINT producto UNIQUE (producto, tienda);